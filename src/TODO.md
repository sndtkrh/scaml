X Big map is not implemented
X comparable type check
X not int nat and nat int-  ``ADD``
X not int nat or nat int -  ``SUB``
X not int nat or nat int -  ``MUL``
X comparable
X-  ``GET``: Access an element in a ``big_map``, returns an optional value to be
X-  ``MEM``: Check for the presence of an element in a ``big_map``.
X-  ``UPDATE``: Assign or remove an element in a ``big_map``.
X no const
Xformat check -  ``address``: An untyped contract address.
Xformat check -  ``key``: A public cryptography key.
Xformat check -  ``key_hash``: The hash of a public cryptography key.
Xformat check-  ``signature``: A cryptographic signature.
X-  ``CREATE_CONTRACT { storage 'g ; parameter 'p ; code ... }``:
