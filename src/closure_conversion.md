
if b then fun () -> x
     else fun () -> 1

(LAMBDA code1, [SOME x])
(LAMBDA code2, [NONE])
